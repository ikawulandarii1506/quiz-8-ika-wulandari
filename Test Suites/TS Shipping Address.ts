<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Shipping Address</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>2173689e-a794-422e-bdfd-c91cc5284047</testSuiteGuid>
   <testCaseLink>
      <guid>4f6e204c-e37b-412e-964d-6a64e45205c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DDT FOR TS/TC Shipping Address</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>ab960954-89dd-47f0-b618-69158e4fb0bc</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Address</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Full Name</value>
         <variableId>52c61478-1aef-446f-88d5-13a7e928dea1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Address Line 1</value>
         <variableId>14ad3c8f-e9b7-455c-ae94-600bb79e7166</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Address Line 2</value>
         <variableId>fff9a9ce-6950-4080-bc17-9dc454c503d3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>City</value>
         <variableId>7ff1957c-266a-47fe-b859-9613f169bcec</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>State/Region</value>
         <variableId>c0fb6a0b-39dc-4d58-939e-5f07191cd359</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Zip Code</value>
         <variableId>52f7e7b0-d1ce-4917-8268-472423edd90c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>ab960954-89dd-47f0-b618-69158e4fb0bc</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Country</value>
         <variableId>179b7f74-cd3b-4c17-88c8-65610d295432</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
