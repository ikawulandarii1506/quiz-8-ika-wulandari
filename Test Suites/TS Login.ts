<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a28e2f2b-5ec7-4724-ae78-2d6e36cf615b</testSuiteGuid>
   <testCaseLink>
      <guid>87904fbf-de90-452f-8e52-b105373f69d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DDT FOR TS/TC Login</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e8ac7d58-0c5d-45c5-b06b-8647c967cfae</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Login</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>e8ac7d58-0c5d-45c5-b06b-8647c967cfae</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>8b1cf37c-75d1-42ae-9882-8fb3981a4645</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e8ac7d58-0c5d-45c5-b06b-8647c967cfae</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>92bf366b-ec4f-4e64-9014-5d3fdda98529</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
