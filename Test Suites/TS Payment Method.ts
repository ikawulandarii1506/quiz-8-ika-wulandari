<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Payment Method</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>f2f3107f-45a6-4a48-b489-f0db80da1e6a</testSuiteGuid>
   <testCaseLink>
      <guid>522b60dd-d35b-4a44-baf8-6b2409c41acf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DDT FOR TS/TC Payment Method</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>e2487519-cc67-40ab-b77f-0af5d027e9dd</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data Payment</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>e2487519-cc67-40ab-b77f-0af5d027e9dd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Full name</value>
         <variableId>aa306983-e847-4890-8ca5-80a4bb63bc59</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e2487519-cc67-40ab-b77f-0af5d027e9dd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>CC Number</value>
         <variableId>ca7ab8fe-ef5d-4386-a4e0-7e01ba3bc520</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e2487519-cc67-40ab-b77f-0af5d027e9dd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>EXP Date</value>
         <variableId>2d157e7b-e09d-44e7-bbe2-36edc860da0e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>e2487519-cc67-40ab-b77f-0af5d027e9dd</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Security Code</value>
         <variableId>e547e696-b97b-4296-b33c-ec80018bb15e</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
