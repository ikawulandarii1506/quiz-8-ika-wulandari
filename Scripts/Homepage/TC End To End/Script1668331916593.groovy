import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('APK/Android-MyDemoAppRN.1.2.0.build-231.apk', false)

Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Homepage/txt-produk'), 3)

Mobile.tap(findTestObject('Homepage/detail-produk'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-add to cart'), 3)

Mobile.tap(findTestObject('Homepage/btn-add to cart'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/jumlah isi keranjang'), 3)

Mobile.tap(findTestObject('Homepage/jumlah isi keranjang'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-proceed to checkout'), 3)

Mobile.tap(findTestObject('Homepage/btn-proceed to checkout'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-login'), 3)

Mobile.setText(findTestObject('Homepage/inpt-username'), 'bob@example.com', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/username is required'), 3)

Mobile.setText(findTestObject('Homepage/inpt-password'), '10203040', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/password is required'), 3)

Mobile.tap(findTestObject('Homepage/btn-login'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-to payment'), 0)

Mobile.setText(findTestObject('Homepage/inp-fullname shipping'), 'Rebecca Winter', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/fullname shipping invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-address line1'), 'mandorley 1', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/addressline1 invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-address line2'), 'entrance', 0)

Mobile.setText(findTestObject('Homepage/inp-city'), 'truro', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/city invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-state'), 'cornwall', 0)

Mobile.setText(findTestObject('Homepage/inp-zip code'), '89750', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/zipcode invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-countri'), 'UK', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/country invalid'), 3)

Mobile.tap(findTestObject('Homepage/btn-to payment'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-review order'), 0)

Mobile.setText(findTestObject('Homepage/inp-fullname cc'), 'Rebecca Winter', 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/fulname cc invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-card number'), '325812657568789', 0)

Mobile.setText(findTestObject('Homepage/inp-exp date'), '03/25', 0)

Mobile.setText(findTestObject('Homepage/inp-security code'), '234', 0)

Mobile.tap(findTestObject('Homepage/btn-rvw order'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-place order'), 0)

Mobile.tap(findTestObject('Homepage/btn-place order'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-countinue shopping'), 0)

Mobile.tap(findTestObject('Homepage/btn-menu toogle'), 0)

Mobile.tap(findTestObject('Homepage/btn-logout'), 0)

Mobile.tap(findTestObject('Homepage/yes logout'), 0)

Mobile.tap(findTestObject('Homepage/btn-OK'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-login'), 3)

Mobile.closeApplication()

