import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('Shipping/TC Shipping Address'), [('nama') : '', ('address1') : '', ('address2') : '', ('city') : ''
        , ('state') : '', ('zipcode') : '', ('country') : ''], FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Homepage/inp-fullname cc'), name, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/fulname cc invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-card number'), cc_number, 0)

Mobile.setText(findTestObject('Homepage/inp-exp date'), exp_date, 0)

Mobile.setText(findTestObject('Homepage/inp-security code'), security_code, 0)

Mobile.tap(findTestObject('Homepage/btn-rvw order'), 0)

