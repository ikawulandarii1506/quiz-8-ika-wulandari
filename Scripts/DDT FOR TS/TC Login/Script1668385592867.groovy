import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('APK/Android-MyDemoAppRN.1.2.0.build-231.apk', false)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Login/btn-menu toogle'), 0)

Mobile.tap(findTestObject('Login/btn-login-menu'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-login'), 3)

Mobile.setText(findTestObject('Homepage/inpt-username'), username, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/username is required'), 3)

Mobile.setText(findTestObject('Homepage/inpt-password'), password, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/password is required'), 3)

Mobile.tap(findTestObject('Homepage/btn-login'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/txt-produk'), 3)

