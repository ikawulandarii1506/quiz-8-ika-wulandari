import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('Product/TC Add Produk To Cart'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Homepage/inp-fullname shipping'), nama, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/fullname shipping invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-address line1'), address1, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/addressline1 invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-address line2'), address2, 0)

Mobile.setText(findTestObject('Homepage/inp-city'), city, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/city invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-state'), state, 0)

Mobile.setText(findTestObject('Homepage/inp-zip code'), zipcode, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/zipcode invalid'), 3)

Mobile.setText(findTestObject('Homepage/inp-countri'), country, 0)

Mobile.verifyElementNotExist(findTestObject('Homepage/country invalid'), 3)

Mobile.tap(findTestObject('Homepage/btn-to payment'), 0)

Mobile.verifyElementExist(findTestObject('Homepage/btn-review order'), 0)

